import React from "react";
// Routes
import RouterConstructor from "./RouterConstructor";
import PublicRoutes from "./PublicRoutes";
import PrivateRoutes from "./PrivateRoutes";

const Routes = ({ valid }) => {
  if (valid) {
    return <RouterConstructor routeList={PrivateRoutes} />;
  }

  return <PublicRoutes />;
};
export default Routes;
