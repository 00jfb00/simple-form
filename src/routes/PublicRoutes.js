import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../pages/notFound";

const PublicRoutes = () => (
  <Switch>
    <Route
      component={NotFound}
    />
  </Switch>
);
export default PublicRoutes;
