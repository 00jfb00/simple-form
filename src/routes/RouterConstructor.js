import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import PropTypes from "prop-types";

const propTypes = {
  userRole: PropTypes.string,
  routeList: PropTypes.arrayOf(PropTypes.shape({
    path: PropTypes.string,
    exact: PropTypes.bool,
    component: PropTypes.func,
    redirect: PropTypes.string,
    capability: PropTypes.string
  }))
};
const defaultProps = {
  userRole: "",
  routeList: []
};
class RouterConstructor extends Component {
  createRoute = route => {
    const {
      path,
      exact,
      name
    } = route;
    const Comp = route.component();

    return (
      <Route
        key={path}
        path={path}
        exact={exact}
        render={props => (
          <>
            <Comp
              {...props}
              name={name}
            />
          </>
        )}
      />
    );
  };

  render() {
    const routeList = this.props.routeList;

    return (
      <Switch>
        {
          routeList.map(route => this.createRoute(route))
        }
        <Redirect to="/" />
      </Switch>
    );
  }
}
RouterConstructor.propTypes = propTypes;
RouterConstructor.defaultProps = defaultProps;
export default RouterConstructor;
