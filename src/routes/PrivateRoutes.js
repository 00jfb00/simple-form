import Form from "../pages/form";

const PrivateRoutes = [
  {
    path: "/",
    exact: true,
    name: "FORM",
    capability: "route:form",
    component: () => Form
  }
];
export default PrivateRoutes;
