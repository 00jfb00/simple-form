const capabilities = {
  guest: [
    "route:intro"
  ],
  user: [
    "route:form"
  ],
  admin: [
    "route:form"
  ]
};
export default capabilities;
