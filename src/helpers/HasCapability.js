import capabilities from "../config/Capabilities";

const check = (rules, role, capability) => {
  const permissions = rules[role];
  if (!permissions) return false;
  if (!permissions.includes(capability)) return false;

  return true;
};
const hasCapability = (role, capability) => check(capabilities, role, capability);
export default hasCapability;
