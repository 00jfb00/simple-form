import axios from "axios";
import { reducers } from "../redux";
import ErrorTransformer from "./ErrorTransformer";

class Interceptor {
  constructor(timeout) {
    this.baseURL = process.env.REACT_APP_API_LOGIN;
    this.client = axios.create({
      baseURL: this.baseURL,
      timeout: timeout || process.env.REACT_APP_TIME_OUT
    });
  }

  getInstance(additionalHeaders = null) {
    return new Promise((resolve, reject) => {
      try {
        const state = reducers.store.getState();
        const form = state.form.data.id;
        this.setClientRequest(false, form, additionalHeaders).then(() => {
          resolve(this.client);
        })
          .catch(err => {
            reject(err);
          });
      } catch (e) {
        reject({ message: "Não foi possível concluir sua solicitação!", details: e.response.message ? e.response.message : "Falha na requisição" });
      }
    });
  }

  setClientRequest(hasToken, token, additionalHeaders = null, isDebugMode) {
    return new Promise((resolve, reject) => {
      let hasError = false;
      this.client.interceptors.response.use(response => {
        response.aux = {
          error: {
            command: `curl -H "Content-Type:application/json" -H "Authorization: ${response.config.headers.Authorization}" "${response.config.baseURL}${response.config.url}"`
          }
        };

        return response;
      }, error => Promise.reject(error));
      this.client.interceptors.request.use(config => {
        config.headers = { "Content-Type": "application/json" };
        if (hasToken) config.headers.Authorization = `Bearer ${token}`;
        if (additionalHeaders) config.headers = { ...config.headers, ...additionalHeaders };

        return config;
      }, error => {
        hasError = true;
        const _error = {};
        _error.code = 0;
        _error.message = "O conteúdo que você está acessando está indisponível no momento. Tente novamente ou volte mais tarde.";
        _error.details = isDebugMode && error.response.message ? error.response.message : "Cabeçalho não pode ser definido na requisição";
        _error.command = `curl -H "Content-Type:application/json" -H "Authorization: ${error.config.headers.Authorization}" "${error.config.baseURL}${error.config.url}"`;

        return Promise.reject(_error);
      });
      this.client.interceptors.response.use(response => response, error => {
        hasError = true;
        let _error = {};
        _error = ErrorTransformer.parse(error, isDebugMode);
        _error.command = `curl -H "Content-Type:application/json" -H "Authorization: ${error.config.headers.Authorization}" "${error.config.baseURL}${error.config.url}"`;

        return Promise.reject(_error);
      });
      if (hasError) {
        reject();
      }
      resolve();
    });
  }
}
export default Interceptor;
