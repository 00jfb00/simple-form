import { type, validate } from "../helpers/InterfaceValidators";

class Feedback {
  constructor(properties = {}) {
    this.title = validate(type("string"), properties.title);
    this.showCloseButton = validate(type("boolean", true), properties.show_close_button);
    this.callbackUrl = validate(type("string"), properties.callback_url);
  }
}
export default Feedback;
