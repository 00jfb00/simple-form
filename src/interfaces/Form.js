import * as _ from "lodash";
import Welcome from "./Welcome";
import Feedback from "./Feedback";
import Question from "./Question";
import { type, validate } from "../helpers/InterfaceValidators";

class Form {
  constructor(properties = {}) {
    this.id = validate(type("string"), properties.uuid);
    this.title = validate(type("string"), properties.title);
    this.activeQuestion = validate(type("number", 0), properties.activeQuestion);
    this.welcome = validate(type("object",
      {},
      ["interface", Welcome]),
    new Welcome(properties.welcome_screens));
    this.feedback = validate(type("object",
      {},
      ["interface", Feedback]),
    new Feedback(properties.thankyou_screens));
    let questions = [];
    if (properties.fields) {
      properties.fields.map(question => {
        const qst = _.cloneDeep(question);
        if (qst.properties) qst.properties.fields = undefined;
        if (question.type === "group") {
          questions = [...questions, qst, ...question.properties.fields];
        } else {
          questions.push(question);
        }
      });
    }
    this.questions = validate(type("array",
      [],
      ["interface", Question]),
    questions.map(field => new Question(field)));
  }

  clone = obj => {
    if (obj === null || typeof obj !== "object") return obj;
    const copy = obj.constructor();
    // eslint-disable-next-line no-restricted-syntax
    for (const attr in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }

    return copy;
  }
}
export default Form;
