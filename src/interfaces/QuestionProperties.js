import QuestionChoice from "./QuestionChoice";
import { type, validate } from "../helpers/InterfaceValidators";

class Question {
  constructor(properties = {}) {
    this.allowMultipleSelection = validate(type("boolean"), properties.allow_multiple_selection);
    this.allowOtherChoice = validate(type("boolean"), properties.allow_other_choice);
    this.showButton = validate(type("boolean", true), properties.show_button);
    this.buttonText = validate(type("string", "Continuar"), properties.button_text);
    this.choices = validate(type("array",
      [],
      ["interface", QuestionChoice]),
    properties.choices ? properties.choices.map(choice => new QuestionChoice(choice)) : undefined);
  }
}
export default Question;
