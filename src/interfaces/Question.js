import QuestionProperties from "./QuestionProperties";
import { type, validate } from "../helpers/InterfaceValidators";

class Question {
  constructor(properties = {}) {
    this.id = validate(type("string"), properties.uuid);
    this.title = validate(type("string"), properties.title);
    this.answer = validate(type("any"), properties.answer);
    this.type = validate(type("string"), properties.type ? properties.type.toUpperCase() : undefined);
    this.validations = validate(type("any", {}), properties.validations);
    this.properties = validate(type("object",
      {},
      ["interface", QuestionProperties]),
    new QuestionProperties(properties.properties));
  }
}
export default Question;
