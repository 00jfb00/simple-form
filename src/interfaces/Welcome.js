import WelcomeAttachment from "./WelcomeAttachment";
import { type, validate } from "../helpers/InterfaceValidators";

class Welcome {
  constructor(properties = {}) {
    this.title = validate(type("string"), properties.title);
    this.attachment = validate(type("object",
      {},
      ["interface", WelcomeAttachment]),
    new WelcomeAttachment(properties.attachment));
  }
}
export default Welcome;
