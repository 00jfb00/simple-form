import { type, validate } from "../helpers/InterfaceValidators";

class Question {
  constructor(properties = {}) {
    this.id = validate(type("string"), properties.uuid);
    this.title = validate(type("string"), properties.label);
  }
}
export default Question;
