import { type, validate } from "../helpers/InterfaceValidators";

class WelcomeAttachment {
  constructor(properties = {}) {
    this.type = validate(type("string"), properties.type);
    this.href = validate(type("string"), properties.href);
  }
}
export default WelcomeAttachment;
