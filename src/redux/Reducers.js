import { combineReducers, createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/es/storage";
import { composeWithDevTools } from "redux-devtools-extension";
import { formReducer as form } from "./form";

const persistConfig = {
  key: "root",
  storage,
  whitelist: [],
  blacklist: ["form"]
};
const combine = combineReducers({
  form
});
const persistedReducer = persistReducer(persistConfig, combine);
const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(ReduxThunk)));
export default {
  store,
  persist: persistStore(store)
};
