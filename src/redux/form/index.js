import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/es/storage";
import dataReducer from "./data/DataReducer";
import dataAction from "./data/DataAction";
import stepReducer from "./step/StepReducer";
import stepAction from "./step/StepAction";

let formReducer = combineReducers({
  data: dataReducer,
  step: stepReducer
});
const formAction = {
  data: dataAction,
  step: stepAction
};
formReducer = persistReducer({
  key: "form",
  storage,
  whitelist: [],
  blacklist: ["step"]
}, formReducer);
export { formReducer, formAction };
