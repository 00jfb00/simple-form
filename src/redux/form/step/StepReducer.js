import * as Step from "./StepAction";

const initialState = {
  type: undefined,
  step: "intro",
  activeQuestion: 0,
  percentage: 0
};
const stepReducer = (state = initialState, action) => {
  switch (action.type) {
    case Step._TYPES._setStep:
      return {
        ...state,
        step: action.step,
        percentage: action.percentage ? action.percentage : state.percentage,
        type: action.type
      };
    case Step._TYPES._setActiveQuestion:
      return {
        ...state,
        percentage: action.percentage ? action.percentage : state.percentage,
        activeQuestion: action.activeQuestion,
        type: action.type
      };
    default:
      return state;
  }
};
export default stepReducer;
