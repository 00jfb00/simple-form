export const _TYPES = {
  _setStep: "STEP::SET_STEP",
  _setActiveQuestion: "STEP::SET_ACTIVE_QUESTION"
};
const action = {};

action.setStep = step => (dispatch, getState) => {
  const questions = getState().form.data.questions;
  const percentage = ((questions.filter(qst => qst.answer !== null).length) * 100) / questions.length;

  dispatch({
    type: _TYPES._setStep,
    percentage,
    step
  });
};

action.setActiveQuestion = activeQuestion => (dispatch, getState) => {
  const questions = getState().form.data.questions;
  const maxQuestions = questions.length;
  const percentage = ((questions.filter(qst => qst.answer !== null).length) * 100) / questions.length;

  if (activeQuestion < maxQuestions) {
    dispatch({
      type: _TYPES._setActiveQuestion,
      percentage,
      activeQuestion
    });
  } else {
    dispatch({
      type: _TYPES._setStep,
      percentage,
      step: "feedback"
    });
  }
};

export default action;
