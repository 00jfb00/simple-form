import Interceptor from "../../../helpers/Interceptor";

export const _TYPES = {
  _authenticate: "DATA::AUTHENTICATE",
  _answerQuestion: "DATA::ANSWER_QUESTION"
};
export const _STATUS = {
  _fetching: "FETCHING",
  _done: "DONE",
  _failed: "FAILED"
};
const action = {};
action.authenticate = () => dispatch => {
  dispatch({
    type: _TYPES._authenticate,
    status: _STATUS._fetching
  });

  new Interceptor().getInstance()
    .then(client => {
      client.get("https://run.mocky.io/v3/719310ec-3cf3-4964-943d-127d8a14066d")
        .then(response => {
          dispatch({
            type: _TYPES._authenticate,
            status: _STATUS._done,
            payload: response.data
          });
        })
        .catch(error => {
          dispatch({
            type: _TYPES._authenticate,
            status: _STATUS._failed,
            error
          });
        });
    })
    .catch(error => {
      dispatch({
        type: _TYPES._authenticate,
        status: _STATUS._failed,
        error
      });
    });
};

action.answerQuestion = (answer, questionIndex) => (dispatch, getState) => {
  const questions = getState().form.data.questions;
  if (Array.isArray(answer) && answer.length === 0) {
    answer = null;
  }
  questions[questionIndex].answer = answer;
  dispatch({
    answer,
    questions,
    type: _TYPES._answerQuestion
  });
};

export default action;
