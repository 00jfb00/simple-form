import * as DataAction from "./DataAction";
import FormInterface from "../../../interfaces/Form";

const initialState = {
  ...new FormInterface({}),
  error: undefined,
  status: undefined,
  type: null
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case DataAction._TYPES._authenticate:
      return {
        ...state,
        status: action.status,
        type: action.type,
        ...DataAction._STATUS._done !== action.status ? {} : new FormInterface(action.payload),
        error: action.error
      };
    case DataAction._TYPES._answerQuestion:
      return {
        ...state,
        status: action.status,
        type: action.type,
        ...{ questions: action.questions },
        error: action.error
      };
    default:
      return state;
  }
};
export default reducer;
