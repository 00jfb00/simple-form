import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-bootstrap";
import Intro from "../../components/Intro";
import Form from "../../components/Form";
import Feedback from "../../components/Feedback";
import ProgressBar from "../../components/ProgressBar";
import { actions } from "../../redux";

class Home extends PureComponent {
  goToNext = () => {
    window.dispatchEvent(new Event("goToNextQuestion"));
  };

  goToPrev = () => {
    window.dispatchEvent(new Event("goToPrevQuestion"));
  };

  render() {
    switch (this.props.form.step) {
      case "intro":
        return (
          <div className="page-intro">
            <Intro />
          </div>
        );
      case "form":
        return (
          <div className="page-form">
            <div className="form-area">
              <div className="form-container">
                <Form />
              </div>
              <div className="overlay-navigation">
                <div className="navigation-area">
                  <div className="navigation-container">
                    <div className="progress-container">
                      <ProgressBar value={this.props.form.percentage} />
                    </div>
                    <NavLink
                      id="prevQuestion"
                      onClick={() => {
                        this.props.setStep("form");
                        this.goToPrev();
                      }}
                      className={`reduced ${this.props.form.activeQuestion === 0 ? "disabled" : ""}`}
                    >
                      <FontAwesomeIcon icon={faChevronUp} />
                    </NavLink>
                    <NavLink
                      id="nextQuestion"
                      onClick={() => {
                        this.props.setStep("form");
                        this.goToNext();
                      }}
                      className={`reduced ${this.props.questions[this.props.form.activeQuestion].answer === null || (this.props.form.activeQuestion >= (this.props.questions.length - 1)) || (this.props.questions[this.props.form.activeQuestion + 1].answer === null && this.props.questions[this.props.form.activeQuestion].answer === null) ? "disabled" : ""}`}
                    >
                      <FontAwesomeIcon icon={faChevronDown} />
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      case "feedback":
        return (
          <div className="page-form">
            <div className="form-area">
              <div className="form-container">
                <Feedback />
              </div>
              <div className="overlay-navigation">
                <div className="navigation-area">
                  <div className="navigation-container">
                    <div className="progress-container">
                      <ProgressBar value={this.props.form.percentage} />
                    </div>
                    <NavLink
                      id="prevQuestion"
                      onClick={() => {
                        this.props.setStep("form");
                      }}
                      className={`reduced ${this.props.form.activeQuestion === 0 ? "disabled" : ""}`}
                    >
                      <FontAwesomeIcon icon={faChevronUp} />
                    </NavLink>
                    <NavLink
                      id="nextQuestion"
                      onClick={() => {
                        this.props.setStep("form");
                      }}
                      className="reduced disabled"
                    >
                      <FontAwesomeIcon icon={faChevronDown} />
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      default: return null;
    }
  }
}
const mapStateToProps = state => ({
  form: state.form.step,
  questions: state.form.data.questions
});
const mapDispatchToProps = dispatch => bindActionCreators({
  setStep: actions.form.step.setStep,
  goToPrevQuestion: actions.form.step.goToPrevQuestion,
  goToNext: actions.form.step.goToNext
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
