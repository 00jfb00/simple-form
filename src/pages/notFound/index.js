import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import {
  Image
} from "react-bootstrap";
import { connect } from "react-redux";
import * as queryString from "query-string";
import { actions } from "../../redux";

class NotFound extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      queryValues: queryString.parse(window.location.search)
    };
  }

  componentDidMount() {
    this.props.authenticate(this.state.queryValues.form);
  }

  render() {
    return (
      <div
        className="page-not-found"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center"
        }}
      >
        <Image style={{ objectFit: "contain", width: "40%" }} src={require("../../assets/img/nicksaude.png")} />
      </div>
    );
  }
}
const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => bindActionCreators({
  authenticate: actions.form.data.authenticate
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NotFound));
