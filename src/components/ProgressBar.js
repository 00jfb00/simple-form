import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { easeQuadInOut } from "d3-ease";
import { ProgressBar } from "react-bootstrap";
import AnimatedProgressProvider from "./AnimatedProgressProvider";
import "react-circular-progressbar/dist/styles.css";

class RegularProgressBar extends PureComponent {
  render() {
    return (
      <AnimatedProgressProvider
        valueStart={0}
        valueEnd={this.props.value}
        duration={1.4}
        easingFunction={easeQuadInOut}
      >
        {value => {
          const roundedValue = Math.round(value);

          return (
            <ProgressBar
              style={{ backgroundColor: `rgba(30, 111, 147, ${roundedValue / 150}) !important` }}
              label={`${roundedValue}%`}
              now={roundedValue}
            />
          );
        }}
      </AnimatedProgressProvider>
    );
  }
}
const mapStateToProps = state => ({
  form: state.form.data.welcome
});
const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RegularProgressBar));
