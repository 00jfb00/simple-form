import React, { Component } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import { actions } from "../redux";

class Question extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.question.answer ? props.question.answer : ""
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    setTimeout(() => {
      const element = document.getElementsByClassName("form-question is-active")[0];
      if (element) element.scrollTo(0, 0);
    }, 500);
    const that = this;
    setTimeout(() => {
      if (that.input && this.props.questionIndex === 0) that.input.focus();
    }, 750);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isActive && this.props.isActive) {
      const that = this;
      setTimeout(() => {
        if (that.input) that.input.focus();
      }, 750);
      window.scrollTo(0, 0);
      setTimeout(() => {
        const element = document.getElementsByClassName("form-question is-active")[0];
        if (element) element.scrollTo(0, 0);
      }, 500);
    }
  }

  render() {
    return (
      <div className={`form-question ${this.props.isActive ? "is-active" : ""} form-question-text`}>
        <span className="title">{`${(this.props.questionIndex + 1)}. ${this.props.question.title} ${this.props.question.validations.required ? " * " : ""}`}</span>
        <InputGroup className="question-text">
          <FormControl
            ref={ref => this.input = ref}
            className="question-text-input"
            value={this.state.value}
            placeholder="Responde aqui..."
            aria-label="answer"
            onKeyPress={event => {
              if (!this.props.isActive) return;

              if (event.key === "Enter" && this.props.question.answer) {
                this.props.goToNext();
              }
            }}
            onChange={evt => {
              this.setState({ value: evt.target.value });
              this.props.answerQuestion(evt.target.value, this.props.questionIndex);
            }}
          />
        </InputGroup>
        {(this.props.question.answer || this.props.question.validations.required === false) && <Button onClick={() => this.props.goToNext()}>OK</Button>}
      </div>
    );
  }
}
const mapStateToProps = () => ({
});
const mapDispatchToProps = dispatch => bindActionCreators({
  answerQuestion: actions.form.data.answerQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));
