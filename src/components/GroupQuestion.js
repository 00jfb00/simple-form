import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { actions } from "../redux";

class Question extends PureComponent {
  componentDidMount() {
    document.addEventListener("keydown", this.handleKeydown, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeydown, false);
  }

  handleKeydown = event => {
    if (!this.props.isActive) return;

    if (event.key === "Enter") {
      this.props.answerQuestion("", this.props.questionIndex);
      setTimeout(() => {
        this.props.goToNext();
      }, 500);
    }
  };

  render() {
    return (
      <div className={`form-question ${this.props.isActive ? "is-active" : ""} form-question-group`}>
        <span className="title">{`${(this.props.questionIndex + 1)}. ${this.props.question.title} ${this.props.question.validations.required ? " * " : ""}`}</span>
        <Button onClick={() => {
          this.props.answerQuestion("", this.props.questionIndex);
          setTimeout(() => {
            this.props.goToNext();
          }, 500);
        }}
        >
          {this.props.question.properties.buttonText}
        </Button>
      </div>
    );
  }
}
const mapStateToProps = () => ({
});
const mapDispatchToProps = dispatch => bindActionCreators({
  answerQuestion: actions.form.data.answerQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));
