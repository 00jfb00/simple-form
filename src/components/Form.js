import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Slider from "react-slick";
import { actions } from "../redux";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import TextQuestion from "./TextQuestion";
import RadioQuestion from "./RadioQuestion";
import CheckboxQuestion from "./CheckboxQuestion";
import DateQuestion from "./DateQuestion";
import GroupQuestion from "./GroupQuestion";

class Form extends PureComponent {
  helpers = {};

  settings = {
    dots: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 250,
    vertical: true,
    touchThreshold: 500,
    verticalSwiping: false,
    focusOnSelect: false,
    centerMode: true,
    cssEase: "linear",
    easing: "linear",
    fade: true,
    nextArrow: null,
    prevArrow: null,
    beforeChange: (current, next) => {
      this.props.setActiveQuestion(next);
      if (next > this.state.lastQuestion) this.setState({ lastQuestion: next });
    }
  };

  pStart = { x: 0, y: 0 };

  pStop = { x: 0, y: 0 };

  constructor(props) {
    super(props);
    this.slide = this.slide.bind(this);
    this.state = {
      lastQuestion: props.activeQuestion,
      counter: 0,
      lastWheelDirection: null
    };
  }

  componentWillMount() {
    // document.addEventListener("touchstart", this.swipeStart);
    // document.addEventListener("touchend", this.swipeEnd);
    // window.addEventListener("wheel", this.onWheel);
  }

  componentDidMount() {
    window.addEventListener("goToNextQuestion", this.slickNext);
    window.addEventListener("goToPrevQuestion", this.slickPrev);
  }

  componentWillUnmount() {
    clearTimeout(this.helpers.timeout);
    this.helpers.timeout = undefined;
    // window.removeEventListener("touchstart", this.swipeStart);
    // window.removeEventListener("touchend", this.swipeEnd);
    // window.removeEventListener("wheel", this.onWheel);

    window.removeEventListener("goToNextQuestion", this.slickNext);
    window.removeEventListener("goToPrevQuestion", this.slickPrev);
  }

  onWheel = e => {
    const direction = this.detectMouseWheelDirection(e);
    if (direction) this.slide(direction);
  }

  swipeStart = e => {
    if (typeof e.targetTouches !== "undefined") {
      const touch = e.targetTouches[0];
      this.pStart.x = touch.screenX;
      this.pStart.y = touch.screenY;
    } else {
      this.pStart.x = e.screenX;
      this.pStart.y = e.screenY;
    }
  }

  swipeEnd = e => {
    if (typeof e.changedTouches !== "undefined") {
      const touch = e.changedTouches[0];
      this.pStop.x = touch.screenX;
      this.pStop.y = touch.screenY;
    } else {
      this.pStop.x = e.screenX;
      this.pStop.y = e.screenY;
    }

    this.swipeCheck();
  }

  swipeCheck = () => {
    const changeY = this.pStart.y - this.pStop.y;
    const changeX = this.pStart.x - this.pStop.x;
    if (this.isPullUp(changeY, changeX)) {
      this.slide("down", true);
    } else if (this.isPullDown(changeY, changeX)) {
      this.slide("up", true);
    }
  };

  isPullDown = (dY, dX) => dY < 0 && (
    (Math.abs(dX) <= 100 && Math.abs(dY) >= 200)
      || (Math.abs(dX) / Math.abs(dY) <= 0.3 && dY >= 40)
  );

  isPullUp = (dY, dX) => dY > 0 && (
    (Math.abs(dX) <= 100 && Math.abs(dY) >= 200)
      || (Math.abs(dX) / Math.abs(dY) <= 0.3 && dY < -40)
  );

  slide = (direction, isMobile = false) => {
    const prevButton = document.getElementById("prevQuestion");
    const nextButton = document.getElementById("nextQuestion");

    if (direction === "up" && (isMobile || (this.state.lastWheelDirection === "up" && this.state.counter >= 15)) && prevButton.className.indexOf("disabled") < 0) {
      this.setState({ counter: 0 });
      if (this.slider) this.slider.slickPrev();
    }
    if (direction === "down" && (isMobile || (this.state.lastWheelDirection === "down" && this.state.counter >= 15)) && nextButton.className.indexOf("disabled") < 0) {
      this.setState({ counter: 0 });
      if (this.slider) this.slider.slickNext();
    }
  };

  detectMouseWheelDirection = e => {
    clearTimeout(this.helpers.timeout);
    this.helpers.timeout = undefined;

    let direction;
    const element = document.getElementsByClassName("form-question is-active")[0];
    if (!element) return;

    const pos = parseInt(element.scrollTop);
    const maxPos = parseInt(element.scrollHeight) - parseInt(element.offsetHeight);

    if (e.deltaY < -40) {
      direction = "up";
      if (this.state.lastWheelDirection === direction) {
        this.setState({ counter: pos === 0 ? this.state.counter + 1 : this.state.counter });
      } else {
        this.setState({ counter: 0 });
      }
    } else if (e.deltaY > 40) {
      direction = "down";
      if (this.state.lastWheelDirection === direction) {
        this.setState({ counter: pos === maxPos ? this.state.counter + 1 : this.state.counter });
      } else {
        this.setState({ counter: 0 });
      }
    }

    if (direction === "up" && e.webkitDirectionInvertedFromDevice) {
      direction = "down";
    } else if (direction === "down" && e.webkitDirectionInvertedFromDevice) {
      direction = "up";
    }
    if (direction) this.setState({ lastWheelDirection: direction });

    const that = this;
    this.helpers.timeout = setTimeout(() => {
      clearTimeout(that.helpers.timeout);
      that.helpers.timeout = undefined;
      that.setState({ counter: 0 });
    }, 1000);

    return direction;
  };

  slickPrev = () => {
    this.slider.slickPrev();
  };

  slickNext = () => {
    this.props.setActiveQuestion(this.props.activeQuestion + 1);
    if (this.slider) this.slider.slickNext();
  };

  render() {
    return (
      <div className="form-container-area">
        <Slider {...this.settings} {...{ initialSlide: this.state.lastQuestion }} style={{ height: "99%" }} ref={slider => this.slider = slider}>
          {this.props.questions.filter((question, index) => index <= this.state.lastQuestion || (question.answer === null && this.props.questions[index - 1] && this.props.questions[index - 1].answer !== null)).map((question, index) => {
            switch (question.type) {
              case "SHORT_TEXT":
                return (
                  <div key={index}>
                    <div>
                      <TextQuestion isActive={index === this.props.activeQuestion} goToPrev={this.slickPrev} goToNext={this.slickNext} question={question} questionIndex={index} />
                    </div>
                  </div>
                );
              case "YES_NO":
                return (
                  <div key={index}>
                    <div>
                      <RadioQuestion isActive={index === this.props.activeQuestion} goToPrev={this.slickPrev} goToNext={this.slickNext} question={question} questionIndex={index} />
                    </div>
                  </div>
                );
              case "MULTIPLE_CHOICE":
                return (
                  <div key={index}>
                    <div>
                      <CheckboxQuestion isActive={index === this.props.activeQuestion} goToPrev={this.slickPrev} goToNext={this.slickNext} question={question} questionIndex={index} />
                    </div>
                  </div>
                );
              case "DATE":
                return (
                  <div key={index}>
                    <div>
                      <DateQuestion isActive={index === this.props.activeQuestion} goToPrev={this.slickPrev} goToNext={this.slickNext} question={question} questionIndex={index} />
                    </div>
                  </div>
                );
              case "GROUP":
                return (
                  <div key={index}>
                    <div>
                      <GroupQuestion isActive={index === this.props.activeQuestion} goToPrev={this.slickPrev} goToNext={this.slickNext} question={question} questionIndex={index} />
                    </div>
                  </div>
                );
              default: return null;
            }
          })}
        </Slider>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  questions: state.form.data.questions,
  activeQuestion: state.form.step.activeQuestion,
  step: state.form.step.step
});
const mapDispatchToProps = dispatch => bindActionCreators({
  setStep: actions.form.step.setStep,
  setActiveQuestion: actions.form.step.setActiveQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Form));
