import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import {
  Button
} from "react-bootstrap";
import { connect } from "react-redux";
import { actions } from "../redux";

class Intro extends PureComponent {
  state = {
    counter: 0,
    lastDirection: undefined,
    canSwipe: false
  };

  helpers = {};

  pStart = { x: 0, y: 0 };

  pStop = { x: 0, y: 0 };

  componentDidMount() {
    const that = this;
    this.helpers.canSwipeTimeout = setTimeout(() => {
      that.setState({ canSwipe: true });
    }, 750);
    // document.addEventListener("touchstart", this.swipeStart);
    // document.addEventListener("touchend", this.swipeEnd);
    // window.addEventListener("wheel", this.onWheel);
  }

  componentDidUpdate(prevProps) {
    if (this.props.activeQuestion !== prevProps.activeQuestion) {
      this.setState({ canSwipe: false }, () => {
        clearTimeout(this.helpers.canSwipeTimeout);
        this.helpers.canSwipeTimeout = undefined;
        const that = this;
        setTimeout(() => {
          that.setState({ canSwipe: true });
        }, 750);
      });
    }
  }

  componentWillUnmount() {
    clearTimeout(this.helpers.canSwipeTimeout);
    this.helpers.canSwipeTimeout = undefined;
    // window.removeEventListener("touchstart", this.swipeStart);
    // window.removeEventListener("touchend", this.swipeEnd);
    // window.removeEventListener("wheel", this.onWheel);
  }

  onWheel = e => {
    const direction = this.detectMouseWheelDirection(e);
    if (direction) this.slide(direction);
  }

  swipeStart = e => {
    if (typeof e.targetTouches !== "undefined") {
      const touch = e.targetTouches[0];
      this.pStart.x = touch.screenX;
      this.pStart.y = touch.screenY;
    } else {
      this.pStart.x = e.screenX;
      this.pStart.y = e.screenY;
    }
  }

  swipeEnd = e => {
    if (typeof e.changedTouches !== "undefined") {
      const touch = e.changedTouches[0];
      this.pStop.x = touch.screenX;
      this.pStop.y = touch.screenY;
    } else {
      this.pStop.x = e.screenX;
      this.pStop.y = e.screenY;
    }

    this.swipeCheck();
  }

  swipeCheck = () => {
    const changeY = this.pStart.y - this.pStop.y;
    const changeX = this.pStart.x - this.pStop.x;
    if (this.isPullDown(changeY, changeX)) {
      this.props.setStep("form");
    }
  };

  isPullDown = (dY, dX) => dY < 0 && (
    (Math.abs(dX) <= 100 && Math.abs(dY) >= 300)
      || (Math.abs(dX) / Math.abs(dY) <= 0.3 && dY >= 60)
  );

  onWheel = e => {
    if (this.detectMouseWheelDirection(e) === "up") {
      this.detectScrollOnTop(e, true);
    }
  };

  detectMouseWheelDirection = e => {
    let delta = null;
    let direction = false;
    if (!e) { // if the event is not provided, we get it from the window object
      e = window.event;
    }
    if (e.wheelDelta) { // will work in most cases
      delta = e.wheelDelta / 60;
    } else if (e.detail) { // fallback for Firefox
      delta = -e.detail / 2;
    }
    if (delta !== null) {
      direction = delta > 0 ? "up" : "down";
    }

    if (direction === "up" && e.webkitDirectionInvertedFromDevice) {
      direction = "down";
    } else if (direction === "down" && e.webkitDirectionInvertedFromDevice) {
      direction = "up";
    }

    return direction;
  };

  detectScrollOnTop = (e, isMobile) => {
    const action = document.getElementById("prevQuestion").className;

    if (this.state.lastDirection !== "top" || action.indexOf("disabled") >= 0) {
      clearTimeout(this.helpers.timeout);
      this.helpers.timeout = undefined;
      this.setState({ counter: 0, lastDirection: "top" });
    }

    const pos = parseInt(document.getElementsByClassName("page-feedback")[0].scrollTop);

    const that = this;
    if (this.helpers.timeout === undefined) {
      this.helpers.timeout = setTimeout(() => {
        clearTimeout(that.helpers.timeout);
        that.helpers.timeout = undefined;
        that.setState({ counter: 0, lastDirection: "top" });
      }, 1000);
    }

    if ((pos < 0 ? 0 : pos) === 0 && this.state.canSwipe) {
      if (this.state.counter >= (isMobile ? 12 : 30)) {
        clearTimeout(this.helpers.timeout);
        this.helpers.timeout = undefined;
        this.props.setStep("form");
        this.setState({ counter: 0, lastDirection: "top" });

        return;
      }

      this.setState({
        counter: this.state.counter + 1,
        lastDirection: "top"
      });
    } else {
      clearTimeout(this.helpers.timeout);
      this.helpers.timeout = undefined;
      this.setState({ counter: 0, lastDirection: "top" });
    }
  };

  render() {
    return (
      <div
        className="page-feedback"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center"
        }}
      >
        <span style={{ fontSize: 26, margin: 20 }}>{this.props.form.title}</span>
        {this.props.form.showCloseButton && (
          <Button onClick={() => {
            window.location.href = this.props.form.callbackUrl;
          }}
          >
            Fechar
          </Button>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  form: state.form.data.feedback,
  activeQuestion: state.form.step.activeQuestion,
  questions: state.form.data.questions
});
const mapDispatchToProps = dispatch => bindActionCreators({
  setStep: actions.form.step.setStep,
  setActiveQuestion: actions.form.step.setActiveQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Intro));
