import React, { Component } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import moment from "moment";
import { connect } from "react-redux";
import { Button, FormControl, InputGroup } from "react-bootstrap";
import { actions } from "../redux";

class Question extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.getInitialValue(props.question.answer ? props.question.answer : ""),
      isValid: moment(this.getInitialValue(props.question.answer ? props.question.answer : ""), "DD/MM/YYYY").isValid() && this.getInitialValue(props.question.answer ? props.question.answer : "").length >= 10
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const that = this;
    setTimeout(() => {
      const element = document.getElementsByClassName("form-question is-active")[0];
      if (element) element.scrollTo(0, 0);
    }, 500);
    setTimeout(() => {
      if (that.input && this.props.questionIndex === 0) that.input.focus();
    }, 750);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isActive && this.props.isActive) {
      const that = this;
      setTimeout(() => {
        if (that.input) that.input.focus();
      }, 750);
      window.scrollTo(0, 0);
      setTimeout(() => {
        const element = document.getElementsByClassName("form-question is-active")[0];
        if (element) element.scrollTo(0, 0);
      }, 500);
    }
  }

  getInitialValue = value => {
    const v = value.replace(/\D/g, "").slice(0, 10);
    if (v.length >= 5) {
      return `${v.slice(0, 2)}/${v.slice(2, 4)}/${v.slice(4)}`;
    }
    if (v.length >= 3) {
      return `${v.slice(0, 2)}/${v.slice(2)}`;
    }

    return v;
  };

  render() {
    return (
      <div className={`form-question ${this.props.isActive ? "is-active" : ""} form-question-date`}>
        <span className="title">{`${(this.props.questionIndex + 1)}. ${this.props.question.title} ${this.props.question.validations.required ? " * " : ""}`}</span>
        <InputGroup className="question-date">
          <FormControl
            ref={ref => this.input = ref}
            type="text"
            className="question-date-input"
            placeholder="DD/MM/YYYY"
            attern="[0-9]*"
            inputMode="numeric"
            min={0}
            value={this.state.value}
            aria-label="answer"
            onKeyUp={() => {
              const v = this.state.value.replace(/\D/g, "").slice(0, 10);
              if (v.length >= 5) {
                this.setState({ value: `${v.slice(0, 2)}/${v.slice(2, 4)}/${v.slice(4)}` });

                return;
              }
              if (v.length >= 3) {
                this.setState({ value: `${v.slice(0, 2)}/${v.slice(2)}` });

                return;
              }
              this.setState({ value: v, isValid: moment(v, "DD/MM/YYYY").isValid() && v.length >= 10 });
            }}
            onKeyPress={event => {
              if (!this.props.isActive) return;

              if (event.key === "Enter" && this.props.question.answer) {
                this.props.goToNext();
              }
            }}
            onChange={evt => {
              this.setState({ value: evt.target.value, isValid: moment(evt.target.value, "DD/MM/YYYY").isValid() && evt.target.value.length >= 10 });
              this.props.answerQuestion(moment(evt.target.value, "DD/MM/YYYY").isValid() && evt.target.value.length >= 10 ? evt.target.value : null, this.props.questionIndex);
            }}
          />
        </InputGroup>
        {this.props.question.answer && this.state.isValid && <Button onClick={() => this.props.goToNext()}>OK</Button>}
      </div>
    );
  }
}
const mapStateToProps = () => ({
});
const mapDispatchToProps = dispatch => bindActionCreators({
  answerQuestion: actions.form.data.answerQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));
