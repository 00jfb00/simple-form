import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button, InputGroup, NavLink } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { actions } from "../redux";

class Question extends PureComponent {
  componentDidMount() {
    window.scrollTo(0, 0);
    setTimeout(() => {
      const element = document.getElementsByClassName("form-question is-active")[0];
      if (element) element.scrollTo(0, 0);
    }, 500);
    document.addEventListener("keydown", this.handleKeydown, false);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isActive && this.props.isActive) {
      window.scrollTo(0, 0);
      setTimeout(() => {
        const element = document.getElementsByClassName("form-question is-active")[0];
        if (element) element.scrollTo(0, 0);
      }, 500);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeydown, false);
  }

  handleKeydown = event => {
    if (!this.props.isActive) return;

    if (event.keyCode >= 65 && event.keyCode < (65 + this.props.question.properties.choices.length)) {
      let answer = [...this.props.question.answer && this.props.question.properties.allowMultipleSelection ? this.props.question.answer : [], event.keyCode - 65];
      if (this.props.question.answer && this.props.question.answer.indexOf(event.keyCode - 65) >= 0) {
        answer = answer.filter(ans => ans !== event.keyCode - 65);
      }
      this.props.answerQuestion(answer, this.props.questionIndex);
      document.activeElement.blur();
      if (this.props.question.properties.allowMultipleSelection === false && (answer !== null && answer.length > 0)) {
        setTimeout(() => {
          this.props.goToNext();
        }, 500);
      }
    }
  };

  returnClass = () => (this.props.question.properties.allowMultipleSelection === false ? "active-dsb" : "active");

  renderChoice = (choice, index) => (
    <NavLink
      key={index}
      id={`question-checkbox-input-option-${index}`}
      className={`question-checkbox-input-option ${this.props.question.answer && this.props.question.answer.indexOf(index) >= 0 ? this.returnClass() : ""}`}
      onClick={() => {
        let answer = [...this.props.question.answer && this.props.question.properties.allowMultipleSelection ? this.props.question.answer : [], index];
        if (this.props.question.answer && this.props.question.answer.indexOf(index) >= 0) {
          answer = answer.filter(ans => ans !== index);
        }
        this.props.answerQuestion(answer, this.props.questionIndex);
        document.activeElement.blur();
        if (this.props.question.properties.allowMultipleSelection === false && (answer !== null && answer.length > 0)) {
          setTimeout(() => {
            this.props.goToNext();
          }, 500);
        }
      }}
    >
      <div className="reduced"><span>{(index + 10).toString(36).toUpperCase()}</span></div>
      <div className="extended">
        <span>
          Tecla&nbsp;
          <span>{(index + 10).toString(36).toUpperCase()}</span>
        </span>
      </div>
      <span>{choice.title}</span>
      <div className="icon-container">
        {this.props.question.answer && this.props.question.answer.indexOf(index) >= 0 && <FontAwesomeIcon icon={faCheck} />}
      </div>
    </NavLink>
  );

  render() {
    return (
      <div className={`form-question ${this.props.isActive ? "is-active" : ""} form-question-checkbox`}>
        <span className="title">{`${(this.props.questionIndex + 1)}. ${this.props.question.title} ${this.props.question.validations.required ? " * " : ""}`}</span>
        <InputGroup className="question-checkbox">
          {this.props.question.properties.allowMultipleSelection === true && <span>Podes selecionar várias opções</span>}
          <div className="question-checkbox-input">
            {this.props.question.properties.choices.map((choice, index) => this.renderChoice(choice, index))}
          </div>
        </InputGroup>
        <Button className={((this.props.question.properties.allowMultipleSelection && this.props.question.answer) || this.props.question.validations.required === false) ? "btn-visible" : "btn-invisible disabled"} ref={ref => this.btn = ref} onClick={() => this.props.goToNext()}>OK</Button>
        <div style={{ height: 75, width: "100%" }} />
      </div>
    );
  }
}
const mapStateToProps = () => ({
});
const mapDispatchToProps = dispatch => bindActionCreators({
  answerQuestion: actions.form.data.answerQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));
