import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Button, InputGroup, NavLink } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { actions } from "../redux";

class Question extends PureComponent {
  componentDidMount() {
    window.scrollTo(0, 0);
    setTimeout(() => {
      const element = document.getElementsByClassName("form-question is-active")[0];
      if (element) element.scrollTo(0, 0);
    }, 500);
    document.addEventListener("keydown", this.handleKeydown, false);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isActive && this.props.isActive) {
      window.scrollTo(0, 0);
      setTimeout(() => {
        const element = document.getElementsByClassName("form-question is-active")[0];
        if (element) element.scrollTo(0, 0);
      }, 500);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeydown, false);
  }

  handleKeydown = event => {
    if (!this.props.isActive) return;

    if (event.keyCode === 83) {
      this.props.answerQuestion(true, this.props.questionIndex);
      document.activeElement.blur();
      setTimeout(() => {
        this.props.goToNext();
      }, 500);
    }
    if (event.keyCode === 78) {
      this.props.answerQuestion(false, this.props.questionIndex);
      document.activeElement.blur();
      setTimeout(() => {
        this.props.goToNext();
      }, 500);
    }
  };

  render() {
    return (
      <div className={`form-question ${this.props.isActive ? "is-active" : ""} form-question-radio`}>
        <span className="title">{`${(this.props.questionIndex + 1)}. ${this.props.question.title} ${this.props.question.validations.required ? " * " : ""}`}</span>
        <InputGroup className="question-radio">
          <div className="question-radio-input">
            <NavLink
              className={`question-radio-input-option ${this.props.question.answer === true ? "active" : ""}`}
              onClick={() => {
                this.props.answerQuestion(true, this.props.questionIndex);
                document.activeElement.blur();
                setTimeout(() => {
                  this.props.goToNext();
                }, 500);
              }}
            >
              <div className="reduced"><span>S</span></div>
              <div className="extended">
                <span>
                  Tecla&nbsp;
                  <span>S</span>
                </span>
              </div>
              <span>Sim</span>
              {this.props.question.answer === true && <FontAwesomeIcon icon={faCheck} />}
            </NavLink>
            <NavLink
              className={`question-radio-input-option ${this.props.question.answer === false ? "active" : ""}`}
              onClick={() => {
                this.props.answerQuestion(false, this.props.questionIndex);
                document.activeElement.blur();
                setTimeout(() => {
                  this.props.goToNext();
                }, 500);
              }}
            >
              <div className="reduced"><span>N</span></div>
              <div className="extended">
                <span>
                  Tecla&nbsp;
                  <span>N</span>
                </span>
              </div>
              <span>Não</span>
              {this.props.question.answer === false && <FontAwesomeIcon icon={faCheck} />}
            </NavLink>
          </div>
        </InputGroup>
        <Button className={this.props.question.validations.required === false ? "btn-visible" : "btn-invisible disabled"} ref={ref => this.btn = ref} onClick={() => this.props.goToNext()}>OK</Button>
        <div style={{ height: 75, width: "100%" }} />
      </div>
    );
  }
}
const mapStateToProps = () => ({
});
const mapDispatchToProps = dispatch => bindActionCreators({
  answerQuestion: actions.form.data.answerQuestion
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Question));
