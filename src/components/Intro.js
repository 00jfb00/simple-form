import React, { PureComponent } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import {
  Button, Image
} from "react-bootstrap";
import { connect } from "react-redux";
import { actions } from "../redux";

class Intro extends PureComponent {
  componentDidMount() {
    document.addEventListener("keydown", this.handleKeydown, false);
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeydown, false);
  }

  handleKeydown = event => {
    if (event.key === "Enter") {
      this.props.setStep("form");
    }
  };

  render() {
    return (
      <div
        className="page-intro"
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center"
        }}
      >
        <Image style={{ objectFit: "contain", width: "40%" }} src={this.props.form.attachment.href} />
        <span style={{ fontSize: 26, margin: 20 }}>{this.props.form.title}</span>
        <Button onClick={() => this.props.setStep("form")}>Começar</Button>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  form: state.form.data.welcome
});
const mapDispatchToProps = dispatch => bindActionCreators({
  setStep: actions.form.step.setStep
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Intro));
