import React, { Component, Fragment } from "react";
import { Router, withRouter } from "react-router-dom";
import "./App.scss";
import { Spinner } from "react-bootstrap";
import * as queryString from "query-string";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { actions } from "./redux";
import Routes from "./routes/Routes";
import history from "./routes/History";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      queryValues: queryString.parse(window.location.search)
    };
  }

  componentDidMount() {
    this.props.authenticate(this.state.queryValues.form);
  }

  renderRouter = (valid = false) => (
    <Router history={history}>
      <Routes valid={valid} />
    </Router>
  );

  render() {
    switch (this.props.form.status) {
      case "FETCHING":
        return (
          <Fragment>
            <div className="loading-container">
              <Spinner animation="grow" role="status" />
            </div>
          </Fragment>
        );
      case "FAILED":
        return this.renderRouter(false);
      default:
        return this.renderRouter(true);
    }
  }
}
const mapStateToProps = state => ({
  form: state.form.data
});
const mapDispatchToProps = dispatch => bindActionCreators({
  authenticate: actions.form.data.authenticate
}, dispatch);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
