import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import { reducers } from "./redux";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<Provider store={reducers.store}>
  <PersistGate loading={null} persistor={reducers.persist}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </PersistGate>
</Provider>, document.getElementById("root"));
serviceWorker.unregister();
